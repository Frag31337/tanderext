﻿
// ==================================================================
// ПРЕДОПРЕДЕЛЕННАЯ ФУНКЦИЯ doAction()
// исполняется при обращении к данному скрипту
// ==================================================================
function doAction() {
    var params = new Object;
    params.idSku = api.context.rowItemId; // Текущий товар (товар в текущей строке документа)
    params.idOutlet = api.context.outletId;   // Текущая торговая точка, с которой ведется работа
    params.idRoute = api.context.routeId;      // Маршрут текущего пользователя МТ
    params.idDoc = api.context.docId;        // Текущий оформляемый документ "Заказ товара"



    var sku = api.skus.sku(params.idSku);
    params.isMerchByQuantity = sku.merchType == 3;

    // Определение рек.количества товара для ед.изм. c коэффициентом 1
    var quantity = calculateRecomendedOrder(params);

    // Приведение  к базовой единице (коэффициент базовой ед.изм., пусть и редко, может отличаться от 1)
    var baseQuantity = quantity; // api.skus.baseUnitFactor(params.idSku);

    // Округление результата и приведение к нулю, если результат отрицательный
    baseQuantity = (baseQuantity > 0 ? Math.round(baseQuantity) : 0);

    // Установка рек.количества товара в документе для базовой ед.изм.
    var recAttr   = api.form.rowAttribute("NameRecommendedQty", params.idSku);
    recAttr.value = baseQuantity;
    recAttr.unit = api.skus.baseUnitId(params.idSku);
}

var  daysForSales = api.constants.value('LastSalesPeriod', '0')

// Определение объема продаж позиции за период daysForSales
//============================================================
function calculateAverageDailySales(params) {
    // Период, по которому определяются среднедневные продажи товара в ТТ - это период, включающий в себя 3 последних отгрузки в ТТ

	// 4. Период в днях, за который анализируется история (из константы)


    var lastVisitDate = api.visit.dateOfLastVisit(params.idOutlet, 0); // Определение даты последнего визита за прошедший период
	var tempdate = Date(lastVisitDate);
var fistVisitDate = new Date(Date.parse(tempdate) - daysForSales * 24 * 60 * 60 * 1000);		
    // 1. Остаток товара в ТТ на начало периода (если с товара не снимается мерч по колличесту, то данные мерча по такому товару не нужны)
    var firstSkuStock = params.isMerchByQuantity ? api.registers.quantityInOutlet(params.idSku, params.idOutlet, fistVisitDate) : 0;

    // 2. Текущий остаток товара в ТТ
    var currentSkuStock = params.isMerchByQuantity ? currentSkuStockInOutlet(params.idSku, params.idOutlet) : 0;
    // 3. Отгрузки товара в ТТ за период
    var skuSalesQuantity = api.registers.periodAmount(params.idSku, params.idOutlet, 2, fistVisitDate, lastVisitDate, params.idDoc);

    // Формула расчетаобъема продаж товара за период:
    // Объем продаж ТТ за преиод = (Остаток товара в ТТ на начало периода - Текущий остаток товара в ТТ + Отгрузки товара в ТТ за период) / Число дней в периоде
    var result = (firstSkuStock - currentSkuStock + skuSalesQuantity); 
    return result;
}

// Определение снятых остатков: если указаны в текущем документе, то с формы документа, иначе из документа последнего посещения
function currentSkuStockInOutlet(idSku, idOutlet) {
    var quantity = 0;
    if (api.form.containsRowAttribute("NameRestBuyPoint")) {
        var recAttr = api.form.rowAttribute("NameRestBuyPoint", idSku); // Попытка получить аттрибут "остаток ТТ" с формы текущего документа
        quantity = recAttr.value > 0 ? (recAttr.value * api.skus.unitFactor(idSku, recAttr.unit)) : 0; // Определение текущего остатка товара из формы документа
    } else {
        quantity = api.registers.quantityInOutlet(idSku, idOutlet); // Определение текущего остатка товара в торговой точке из валидированного документа
    }

    return quantity;
}

//определение рек.количества товара для ед.изм. c коэффициентом 1
//============================================================
function calculateRecomendedOrder(params) {
    // 1. Определение среднедневного объема продаж товара в торговой точке
    var VolumeSales = calculateAverageDailySales(params);
		//api.log.info("VolumeSales:"+VolumeSales);
		//api.log.info("daysForSales:"+daysForSales);
    // 2. Определение количества дней до следующего визита в торговой точке
	   // var lastVisitDate = api.visit.dateOfLastVisit(params.idOutlet, 0); // Определение даты последнего визита за прошедший период
//	var tempdate = Date(lastVisitDate);
	//api.log.info("tempdate:"+tempdate);
	//var today = date();
  //  var daysToNextVisit = (Date.parse(Date()) - Date.parse(tempdate))/( 24 * 60 * 60 * 1000);
	//	api.log.info("daysToNextVisit:"+daysToNextVisit);
    // 3. Определение текущего остатка товара в торговой точке (с учетом значения, зафиксированного во время текущего визита)
	var time=new Date();
var thismonth=time.getMonth();
	var monthconst = "seasonkoeff" + thismonth; // получить дату через джс
	var sesonkoeff = api.constants.value(monthconst, '0') ;
		//api.log.info("time:"+time);
			//api.log.info("monthconst:"+monthconst);
						//api.log.info("sesonkoeff:"+sesonkoeff);
    // Формула расчета рек.заказа:

   	if (VolumeSales > 0){
   	var result = (VolumeSales / daysForSales) * 7;//(daysToNextVisit + 1) * seasonkoeff;
   }
   else    // Если продаж нет рекомендуем одну упаковку или кегу
   {
var result = 1
   };
   	//api.log.info("result:"+result);
    return result;

}
