﻿
//===================================================================
//СКРИПТ, ВЫЗЫВАЕМЫЙ ПО СОБЫТИЮ "preopen" для "Заказа товара" (preorder)
//===================================================================
// = b020_36. Добавление фильтра товаров онлайн                     =
//===================================================================
// Добавим фильтр "Непрокупленные товары",
// включающий товары определенного набора, отсутствующие 
// в отгрузках текущего месяца (или сегодняшних заказах)"

var filterNames = "'Core', 'Must stock list'";

var skusQuantityForMessage = 8; //максимальное количество непроданных обязательных товаров, названия которых следует выводить в предупреждении

var isTestMode = false;

// ==================================================================
// ПРЕДОПРЕДЕЛЕННАЯ ФУНКЦИЯ doAction() 
// исполняется при обращении к данному скрипту
// ==================================================================
function doAction() {


    api.result.type = "ok";
    api.result.showMessage = false;



    var iSQL = "select prt.id as [PLid],prt.Name as [PLname],pat.id as [PTid],pat.name as [PTname],pl.Price as [Price] from refPriceTypes as prt " +
        "join refPriceList as pl on pl.idPriceType = prt.id prt " +
        "join refPayTypes as pat on pat.id = pl.idPayType"

    api.sql.exec(iSQL);

    var raw = api.sql.rows()

    raw.forEach(function (item) {


    })

}