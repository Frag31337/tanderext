﻿
//===================================================================
//СКРИПТ, ВЫЗЫВАЕМЫЙ ПО СОБЫТИЮ "presave" для "Заказа товара" (preorder)
//===================================================================
// = c060_36. Предупреждение о незаказанных товарах                 =
//===================================================================
// В данном примере проверяется наличие в отгрузках текущего месяца 
// (или сегодняшних заказах) всех товаров, которые включены в фильтр

var filterNames = "'Core', 'Must stock list'";

var skusQuantityForMessage = 8; //максимальное количество непроданных обязательных товаров, названия которых следует выводить в предупреждении

var isTestMode = false;

// ==================================================================
// ПРЕДОПРЕДЕЛЕННАЯ ФУНКЦИЯ doAction() 
// исполняется при обращении к данному скрипту
// ==================================================================
function doAction() {
    
    api.result.type = "ok";
    api.result.showMessage = false;
    
    //набор необходимых параметров из контекста
    var params = new Object();
    params.outletId = api.context.idOutlet;
    params.positionId = api.context.idPosition;
    params.distributorId = api.context.idDistributor;
    params.docId = api.context.idDoc;

    // ==================================================================
    // Проверка на превышение 25% лимита ДЗ
    // ==================================================================

    var summ = getAmountDoc(params);
    var limit = getCreditlimit(params);
    var limit25 = limit / 4;

    if (!isNaN(limit) && limit25 < summ && limit != 0) {
        dialogTitle = 'Сумма заказа первысила 25% от лимита кредита!'; //выведем в заголовок диалога название точки
        dialogText = "Сумма заказа первысила 25% от лимита кредита: \n";
        dialogText += "Сумма заказа равна:" + summ.toFixed(2) + "\n";
        dialogText += "25% от лимита кредита равены:" + limit25.toFixed(2) + "\n";
        dialogText += "Лимит кредита равен:" + limit.toFixed(2) + "\n";
        dialogText += "\n";


        var answer = api.interactive.question(dialogTitle, dialogText, "Ok", "Ok");
    }

    // ==================================================================
    // Проверка на превышение суммы среднего заказа
    // ==================================================================

    var avgsumorder = getavgsumm(params);
    api.log.info('avgsumorder : ' + avgsumorder);
    api.log.info('summ: ' + summ);
    if (!isNaN(avgsumorder) && avgsumorder < summ) {
        var raznsum = avgsumorder - summ;
        dialogTitle = 'Сумма заказа первысила cреднюю сумму заказа по этой ТТ';
        dialogText = "Сумма заказа первысила cреднюю сумму заказа по этой ТТ на:" + raznsum.toFixed(2) + "\n";
        dialogText += "Сумма заказа равна:" + summ.toFixed(2) + "\n";
        dialogText += "Средняя сумма заказа равна:" + avgsumorder.toFixed(2) + "\n";
        dialogText += "\n";
        var answer = api.interactive.question(dialogTitle, dialogText, "Ok", "Ok");
    }
    
    //перечисление ссылок на фильтры, товары из которых должны быть проданы в точку
    var filterId   = getFilterId(params, filterNames);
    
    //если искомых фильтров в контексте текущей точки не найдено, ничего делать не требуется
    if (filterId.length == 0) {
        return;
    }
    
    var unSoldSkus = getUnSoldSkus(params, filterId);
    unSoldSkus.sort(sortArrayByTitle);
    
    //1. Если есть неотгружавшиеся с начала месяца товары определенного фильтра, предупредим об этом пользователя
    if (unSoldSkus.length > 0) {
        
        var dialogTitle = api.outlets.title(api.context.idBuyPoint); //выведем в заголовок диалога название точки
        
        var skusText = "";
        for (var i = 0; i < unSoldSkus.length; ++i) {
            if (i == skusQuantityForMessage) {
                skusText += "\nи еще " + (unSoldSkus.length - skusQuantityForMessage) + " поз. из '" + filterNames + "'\n";
                break;
            }
            skusText += (i == 0 ? "" : ",\n") + "- " + unSoldSkus[i].title;
        }
        if (unSoldSkus.length <= skusQuantityForMessage) {
            skusText += "\n";
        }
        
        var dialogText = "Товары из Фокусного ассортимента \n";
        //var dialogText = "" + unSoldSkus.length + " поз. из '" + filterNames + "' не были отгружены в точку в этом месяце:\n";
        dialogText += skusText;
        dialogText += "не были отгружены в точку в этом месяце!\n";
        dialogText += "\n";


        
        var answer = api.interactive.question(dialogTitle, dialogText, "Ok", "Ok");
        

    }
}

// ==================================================================
// ДОПОЛНИТЕЛЬНЫЕ ФУНКЦИИ
// ==================================================================

// возвращает определенный набор фильтров
function writeToLog(str, params) {
    
    if (!isTestMode) {
        return;
    }
    
    var prefix = "[PREORDER/PRESAVE]: ";
    
    if (arguments.length < 2) {
        api.log.info(prefix + str);
        return;
    }
    
    if (params.type == "error") {
        if (params.interactive) { api.interactive.error(str); }
        api.log.error(prefix + str);
    } else if (params.type == "warning") {
        api.log.warn(prefix + str);
        if (params.interactive) { api.interactive.warning(str); }
    } else {
        api.log.info(prefix + str);
        if (params.interactive) { api.interactive.info(str); }
    }
}

// упорядочивает массив по значениям в реквизите "title"
function sortArrayByTitle(i, j) {
    
    if (i["id"] == 0 || j["id"] == 0)
        return 0;
    if (i["title"] > j["title"])
        return 1;
    else if (i["title"] < j["title"])
        return -1;
    else
        return 0;
};

// ==================================================================
// ФУНКЦИИ ВЫБОРКИ ИЗ БАЗЫ ДАННЫХ
// ==================================================================

function getAmountDoc(params) {

    var strQuery = "select amount from docjournal where idbuypoint = " + params.outletId + " and id = " + params.docId;
    api.log.info('Выбираем сумму у черновика документа: ' + strQuery);
    if (api.sql.exec(strQuery)) {
        while (api.sql.next()) {
            var result = api.sql.value("amount");
        }
    }


    return result;
}

function getCreditlimit(params) {

    var strQuery = "select creditLimit from refbuypoints where id = " + params.outletId;

    if (api.sql.exec(strQuery)) {
        while (api.sql.next()) {
            var result = api.sql.value("creditLimit");
        }
    }
    api.log.info('Выбираем лимит кредита ТТ: ' + strQuery);
    api.log.info('лимит кредита ТТ равен: ' + result);
    return result;
}

function getavgsumm(params) {

    var strQuery = "select count (p.id) as kolvo from dhpreorder p join docjournal dj on dj.id = p.id where dj.idoperation = 18 and dj.idstate != 1 and p.idbuypoint = " + params.outletId;

    if (api.sql.exec(strQuery)) {
        while (api.sql.next()) {
            var kolvo = api.sql.value("kolvo");
        }
    }
    api.log.info('Выбираем кол-во сборов заказа: ' + strQuery);
    api.log.info('Кол-во сборов заказа: ' + kolvo);
    var strQuery = "select sum (amount)  as amountsum  from drpreorder where iddoc in (select p.id from dhpreorder p join docjournal dj on dj.id = p.id where dj.idoperation = 18 and dj.idstate != 1 and p.idbuypoint = " + params.outletId + ") and idgoods like '2%'";

    if (api.sql.exec(strQuery)) {
        while (api.sql.next()) {
            var amountsum = api.sql.value("amountsum");
        }
    }
    api.log.info('Выбираем сумму по всем сборам заказа: ' + strQuery);

    var result = amountsum / kolvo;
    api.log.info('Средняя сумма заказа в ТТ: ' + result);
    return result;
}


// возвращает определенный набор фильтров
function getFilterId(params, filterNames) {
    
    var result = new Array();

    var strQuery = "SELECT f.id AS [filterId] FROM refFilters as f \n" +
    "join refsets as s on s.[idItem] = f.[id] \n" +
    "join refbuypointsets as bs on bs.[idSet] = s.[id] \n" +
    "join refBuypoints as b on b.idClassifier5 = bs.idClassifier5 \n" +
    "WHERE f.deleted = 0 \n" +
    "and b.id = " + params.outletId + " and f.name in (" + filterNames + ") \n"

    if (api.sql.exec(strQuery)) {
        while (api.sql.next()) {
            result.push(api.sql.valueToId("filterId"));
        }
    }
    
    return result;
}

// возвращает список неотгружавшихся в точку с начала месяца товаров
function getUnSoldSkus(params, filterId) {
    
    //1. Список товаров, включенных в заданные фильтры
    var filteredSkus = new Array();
    
    var skusByOneFilter;
    for (var i = 0; i < filterId.length; i++) {
        skusByOneFilter = api.sets.idSkusByItem(filterId[i]);
        for (var j = 0; j < skusByOneFilter.length; j++) {
            if (api.skus.get(skusByOneFilter[j]).idSkuCO == 0) {
                filteredSkus.push(skusByOneFilter[j]);
            }
        }
    }
    writeToLog("filteredSkus.length = " + filteredSkus.length);
    
    //2. Список товаров, проданных в точку с начала месяца или заказанных в ней сегодня
    var soldSkus = new Array();
    
    var strQuery = "SELECT \n" +
    "    DISTINCT dr.idGoods AS [idSku] \n" +
    "FROM docJournal dj \n" +
    "INNER JOIN drSales dr ON dr.idDoc = dj.id AND dr.quantity > 0 \n" +
    "WHERE dj.deleted = 0 AND dj.idStatus = 1 \n" +
    "    AND DATE(dj.OpDate, 'start of month') = DATE('now', 'localtime', 'start of month') \n" +
    "    AND dj.idBuypoint = " + params.outletId + " \n" +
    "UNION ALL \n" +
    "SELECT \n" +
    "    DISTINCT dr.idGoods AS [idSku] \n" +
    "FROM docJournal dj \n" +
    "INNER JOIN drPreorder dr ON dr.idDoc = dj.id AND dr.quantity > 0 \n" +
    "WHERE dj.id = " + params.docId + " OR ( \n" +
    "    dj.deleted = 0 \n" +
    "    AND DATE(dj.OpDate) = DATE('now', 'localtime') \n" +
    "    AND dj.idBuypoint = " + params.outletId + " \n" +
    "    AND dj.idStatus = 1 \n" +
    "    AND dj.id != (SELECT idPreviousVersion FROM DocJournal WHERE id = " + params.docId + ") \n" +
    "    )";
    
    if (api.sql.exec(strQuery)) {
        while (api.sql.next()) {
            soldSkus.push(api.sql.valueToId("idSku"));
        }
    }
    writeToLog("soldSkus.length = " + soldSkus.length);
    
    //3. Список товаров, доступных для продажи в точке
    //   (чтобы пропускать товары искомых фильтров, которые не могут быть проданы в точке в принципе)
    var availableSkus = api.form.rowItemIdList();
    
    writeToLog("availableSkus.length = " + availableSkus.length);
    
    //4. Список товаров, не проданных в данную ТТ, но состоящих  в фильтрах МСЛ и Листинг либо заполненных в текущем документе
    var unSoldSkus = new Array();
    
    var isFound; //флаг того, что товар уже был продан/заказан
    
    //перебор товаров, включенных в заданные фильтры
    for (var iF = 0; iF < filteredSkus.length; iF++) {
        var finded = false;
        
        // поиск совпадения среди товаров, проданных в точку с начала месяца или заказанных в ней сегодня
        isFound = false;
        for (var iS = 0; iS < soldSkus.length; iS++) {
            if (soldSkus[iS] == filteredSkus[iF] || api.skus.get(soldSkus[iS]).idSkuCO == filteredSkus[iF]) {
                isFound = true;
                break;
            }
        }
        
        //если совпадение не найдено, товар добавляется в список непроданных
        if (!isFound) {
            //перед сохранением документа следует обязательно проверить вхождение непрокупленных позиций в ассортимент точки
            for (var iA = 0; iA < availableSkus.length; iA++) {
                if (availableSkus[iA] == filteredSkus[iF] || api.skus.get(availableSkus[iA]).idSkuCO == filteredSkus[iF]) {
                    unSoldSkus.push({id: filteredSkus[iF], title: api.skus.title(filteredSkus[iF])});
                    break;
                }
            }
        }
    }
    writeToLog("unSoldSkus.length = " + unSoldSkus.length);
    
    return unSoldSkus;
}
