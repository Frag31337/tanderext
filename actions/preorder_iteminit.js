﻿
//============================================================
//СКРИПТ, ВЫЗЫВАЕМЫЙ ПО СОБЫТИЮ "iteminit" для "Заказа"
//(для версии МТ 3.0.27 и выше)
//============================================================
//если остаток более "3 уп.", то он округляется до целого в меньшую сторону
//если остаток менее "3 уп.", то выводится значение остатка, пересчитанное в "шт." (базовые ед.изм.)
//значение "3 уп." определяется из константы "SKUStockToConvert"
//
//операция выполняется, только в том случае, если в константе "SKUStockToConvert" указано значение больше 0
//============================================================

// ==================================================================
// ПРЕДОПРЕДЕЛЕННАЯ ФУНКЦИЯ doAction()
// исполняется при обращении к данному скрипту
// ==================================================================
function doAction() {
	api.result.type = "ok";
	api.result.showMessage = false;

    api.log.info('Activated');
    
}

// ==================================================================
// Дополнительные ФУНКЦИИ
// ==================================================================

//получение информации о базовой единице измерения товара по его id
//==================================================================
function getBaseUnit(idSKU) {

	var result = new Object;
	result.isCorrect = false;

	strQuery = "SELECT \n" +
		"   skus.idBaseUnitMT AS [idUnit], \n" +
		"   sku_un.factor AS [factor] \n" +
		"FROM refGoods skus \n" +
		"INNER JOIN refGoodsUnits sku_un ON sku_un.idGoods = skus.id AND sku_un.idUnit = skus.idBaseUnitMT \n" +
		"WHERE skus.deleted = 0 AND skus.id = " + idSKU + " \n" +
		"LIMIT 1;";
	api.sql.exec(strQuery);

	if (api.sql.next()) {
		result.isCorrect = true;
		result.idUnit = api.sql.valueToId("idUnit");
		result.factor = api.sql.value("factor");
	}

	return result;
}
